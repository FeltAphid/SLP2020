﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealUp : MonoBehaviour//handles information of the health pickup prefab
{
	public int index;

	public GameObject myObject;

	public void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			PlayerController.playerHealthChange = PlayerController.playerHealthChange + 1;
			Destroy(myObject);
		}
	}


}

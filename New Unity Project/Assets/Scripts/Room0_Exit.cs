﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Room0_Exit : MonoBehaviour {//handles transitioning between rooms


	static int rnd;
	private void Start()
	{
	}


	public int index;

	public CircleCollider2D Player;
	public BoxCollider2D myObject;

	public void OnCollisionEnter2D(Collision2D collision){
		if (collision.gameObject.tag == "Player")
		{
			PlayerController.sceneCounter++;
			if(PlayerController.sceneCounter < 5) {
				SceneManager.LoadScene(PlayerController.room1, LoadSceneMode.Single);
			}
			else
			{
				SceneManager.LoadScene("End", LoadSceneMode.Single);
			}
			
		}
	}
}


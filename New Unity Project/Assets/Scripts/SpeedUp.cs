﻿using UnityEngine;
using System.Collections;

public class SpeedUp : MonoBehaviour {//handles information of the SpeedUp prefab

		

	public int index;
	
	public GameObject myObject;

	public void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.tag == "Player")
		{
			PlayerController.MOVEMENT_BASE_SPEED = PlayerController.MOVEMENT_BASE_SPEED + 0.5f;
			Destroy(myObject);
		}
		
	}
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootEnemy : MonoBehaviour// handles necisary information and basic AI for the Eye enemy
{
    public GameObject player;
    public GameObject enemy;
    public GameObject eyeShotPrefab;
    public float PROJECTILE_BASE_SPEED = 5.0f;
    public float healthChange = 5.0f;

    //potential drop prefabs
    public GameObject speedPrefab;
    public GameObject healthPrefab;


    int drop;
    private void Start()
    {
        drop = Random.Range(0, 7);
        InvokeRepeating("Shoot", 0.5f, 2.0f);
    }

    public void Shoot(){
        Vector2 shootingDirection = player.transform.localPosition - enemy.transform.localPosition/* + new Vector3(-3,0,0)*/;
        shootingDirection.Normalize();

        GameObject eyeShot = Instantiate(eyeShotPrefab, transform.position, Quaternion.identity) as GameObject;
        EyeShot eyeShotScript = eyeShot.GetComponent<EyeShot>();
        eyeShotScript.velocity = shootingDirection * PROJECTILE_BASE_SPEED;
        eyeShotScript.Enemy = gameObject;
        eyeShot.transform.Rotate(0, 0, Mathf.Atan2(shootingDirection.y, shootingDirection.x) * Mathf.Rad2Deg);
        Destroy(eyeShot, 3.0f);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == ("Projectile"))
        {
            Destroy(collision.gameObject);
            healthChange--;
            if (healthChange <= 0)
            {
                if (drop == 0)
                {
                    GameObject speedUp = Instantiate(speedPrefab, transform.position, Quaternion.identity) as GameObject;
                }
                if (drop == 1)
                {
                    GameObject healUp = Instantiate(healthPrefab, transform.position, Quaternion.identity) as GameObject;
                }
                Destroy(enemy);
            }
        }
    }
 }


﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour // handles all movement and interactions from the player to the game
{
	[Header("Input settings:")]
	public bool useController;


	[Header("Character attributes:")]
	public static float MOVEMENT_BASE_SPEED = 5.0f;
	public float CROSSHAIR_DISTANCE = 1.0f;
	public float PROJECTILE_BASE_SPEED = 1.0f;
	public float SHOOTING_COOLDOWN_TIME = 1.0f;
	public float BASE_HEALTH = 3.0f;

	//[Space 1]
	[Header("Character statistics:")]
	public Vector2 movementDirection;
	public float movementSpeed;
	public bool endOfAiming;
	public float shootCooldown = 0;
	public static float playerHealthChange = 0;
	public float currentPlayerHealth;

	//[Space 1]
	[Header("references")]
	public Animator animator;
	public Rigidbody2D rb;
	public GameObject crosshair;
	public GameObject speedPickup;
	public CircleCollider2D playerCollider;
	public BoxCollider2D Sp;
	public Text healthText;

	//[Space 1]
	[Header ("Prefabs:")]
	public GameObject projectilePrefab;
	public GameObject speedPrefab;
	public GameObject healPrefab;

	public static int room1;
	public static int room4;
	public static int room3;
	public static int room2;

	public static int sceneCounter = 0;


	// Use this for initialization
	void Start () {

		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Confined;

		room1= Random.Range(1, 4);
		do
		{
			room2 = Random.Range(1, 4);
		}
		while (room2 == room1);
		do
		{
			room3 = Random.Range(1, 4);
		}
		while (room3 == room2 || room3 == room1);

		healthText.text = ("Health: " + currentPlayerHealth);
	}
	
	// Update is called once per frame
	void Update () {
		ProcessInputs ();//processes keys pressed by the player and determines actions in game
		Move ();// moves the player character based on inputs
		Animate ();// animates the player character (no animations, just code framework)
		Aim ();//determines the position of the crosshair from the mouse.
		shoot ();//creates projectile prefab with information from Aim
		Pickup();// debug tool to create objects
		Health();//handles the player health and death
	}

	void ProcessInputs(){
		Physics2D.gravity = Vector2.zero;

		if (useController) {

		
			movementDirection = new Vector2 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"));
			movementSpeed = Mathf.Clamp (movementDirection.magnitude, 0.0f, 1.0f);
			movementDirection.Normalize ();
		
			endOfAiming = Input.GetButtonUp ("Fire1");
		} else {
			Vector2 mouseMovement = transform.position;
			if (Input.GetKey ("w")) {
				mouseMovement.y += MOVEMENT_BASE_SPEED * Time.deltaTime;
			}
			if (Input.GetKey ("s")) {
				mouseMovement.y -= MOVEMENT_BASE_SPEED * Time.deltaTime;
			}
			if (Input.GetKey ("d")) {
				mouseMovement.x += MOVEMENT_BASE_SPEED * Time.deltaTime;
			}
			if (Input.GetKey ("a")) {
				mouseMovement.x -= MOVEMENT_BASE_SPEED * Time.deltaTime;
			}
			transform.position = mouseMovement;
		
			mouseMovement.Normalize ();

			movementSpeed = Input.GetAxis ("Vertical");
			endOfAiming = Input.GetButtonUp ("Fire1");


		}

			if (endOfAiming) {
				shootCooldown = SHOOTING_COOLDOWN_TIME;
			}
			if (shootCooldown > 0.0f) {
				shootCooldown -= Time.deltaTime;
		}



	}

	void Move(){
		rb.velocity = (7 * (movementDirection * movementSpeed)) * MOVEMENT_BASE_SPEED;

	}
	void Animate () {
		animator.SetFloat ("Horizontal", movementDirection.x);
		animator.SetFloat ("Vertical", movementDirection.y);
	}

	void Aim() {
		Vector2 mouse = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
		mouse = Camera.main.ScreenToWorldPoint (mouse);
		crosshair.transform.position = new Vector2(mouse.x, mouse.y);

	}

	void Health()
	{
		currentPlayerHealth = currentPlayerHealth + playerHealthChange;
		playerHealthChange = 0;
		if (currentPlayerHealth > BASE_HEALTH){
			currentPlayerHealth = BASE_HEALTH;
		}
		healthText.text = ("Health: " + currentPlayerHealth);
		if (currentPlayerHealth <= 0){
			SceneManager.LoadScene("death", LoadSceneMode.Single);
		}
	}


	void shoot(){
		Vector2 shootingDirection = crosshair.transform.localPosition + new Vector3(-1, -2,0);
		shootingDirection.Normalize ();

		if (endOfAiming) {
			GameObject projectile = Instantiate(projectilePrefab, new Vector2(transform.position.x +1, transform.position.y+2 ) , Quaternion.identity)as GameObject;
			Projectile projectileScript = projectile.GetComponent<Projectile>();
			projectileScript.velocity = shootingDirection * PROJECTILE_BASE_SPEED;
			projectileScript.Player = gameObject;
			projectile.transform.Rotate(0, 0, Mathf.Atan2(shootingDirection.y, shootingDirection.x) * Mathf.Rad2Deg);
			Destroy (projectile, 1.0f);
		}
	}

	void Pickup(){
		if (Input.GetKey("p")){
			GameObject speedUp = Instantiate(speedPrefab, transform.position, Quaternion.identity) as GameObject;
		}
		if (Input.GetKey("h")){
			GameObject healUp= Instantiate(healPrefab, transform.position, Quaternion.identity) as GameObject;
		}
	}

	


}

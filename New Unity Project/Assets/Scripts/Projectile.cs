﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {//handles moving the projectile prefab, as well as any interactions.

	public Vector2 velocity = new Vector2(0.0f, 0.0f);
	public GameObject Player;

	void Update() {
		Vector2 currentPosition = new Vector2(transform.position.x, transform.position.y);
		Vector2 newPosition = currentPosition + velocity * Time.deltaTime;

		Debug.DrawLine(currentPosition, newPosition, Color.red);

		RaycastHit2D[] hits = Physics2D.LinecastAll(currentPosition, newPosition);

		foreach (RaycastHit2D hit in hits) {
			GameObject other = hit.collider.gameObject;
			if (other != Player) {
				if (other.CompareTag("Target")) {
					Debug.Log("You hit the Target!");
					Destroy(gameObject);
					break;
				}
				
			}
		}
	


		transform.position = newPosition;
	}

}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Right_Door : MonoBehaviour {
	
	public int index;

	public CircleCollider2D Player;
	public BoxCollider2D myObject;

	public void OnCollisionEnter2D(Collision2D collision){
		SceneManager.LoadScene("Rogouelike updated", LoadSceneMode.Single);
	}
}


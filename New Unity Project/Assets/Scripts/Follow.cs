﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour//handles necisary information, and basic AI for the ghost enemy
{
    float speed = 3.5f;
    public GameObject player;
    public GameObject enemy;
    public GameObject playerShot;
    public int healthChange = 5;
    static int drop;

    //potential drop prefabs
    public GameObject speedPrefab;
    public GameObject healthPrefab;
 

    // Start is called before the first frame update
    void Start()
    {
        drop = Random.Range(0,7);
    }

    // Update is called once per frame
    void Update()
    {
        enemy.transform.position = Vector2.MoveTowards(enemy.transform.position, player.transform.position, speed * Time.deltaTime);
    }

    public void OnCollisionEnter2D(Collision2D collision){
        if (collision.gameObject.tag == ("Player"))
        {
            PlayerController.playerHealthChange = PlayerController.playerHealthChange - 1;
        }
        if (collision.gameObject.tag ==("Projectile"))
        {
            Destroy(collision.gameObject);
            healthChange--;
            if (healthChange <= 0)
            {
                if (drop == 0)
                {
                    GameObject speedUp = Instantiate(speedPrefab, transform.position, Quaternion.identity) as GameObject;
                }
                if (drop == 1)
                {
                    GameObject healUp = Instantiate(healthPrefab, transform.position, Quaternion.identity) as GameObject;
                }
                Destroy(enemy);
            }
        }
    }

    void health()
    {
        //currentHealth = currentHealth + healthChange;
        //healthChange = 0;
        //if (healthChange == 0)
        //{
            Debug.Log("enemy destroyed");
            if(drop == 0)
            {
                GameObject speedUp = Instantiate(speedPrefab, transform.position, Quaternion.identity) as GameObject;
            }
            if (drop == 1)
            {
                GameObject healUp = Instantiate(healthPrefab, transform.position, Quaternion.identity) as GameObject;
            }
        //}
            Destroy(enemy);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeShot : MonoBehaviour//handles movement and interactions of the eyeShot prefab fired by the eye enemy
{
    public Vector2 velocity = new Vector2(0.0f, 0.0f);
    public GameObject Enemy;

    // Update is called once per frame
    void Update()
    {
		Vector2 currentPosition = new Vector2(transform.position.x, transform.position.y);
		Vector2 newPosition = currentPosition + velocity * Time.deltaTime;

		Debug.DrawLine(currentPosition, newPosition, Color.red);

		RaycastHit2D[] hits = Physics2D.LinecastAll(currentPosition, newPosition);

		foreach (RaycastHit2D hit in hits)
		{
			GameObject other = hit.collider.gameObject;
			if (other.tag != "FollowEnemy" || other.tag != "ShootEnemy")
			{
				if (other.CompareTag("Player"))
				{
					PlayerController.playerHealthChange--;
					Destroy(gameObject);
					break;
				}

			}
		}



		transform.position = newPosition;
	}
}
